#! /bin/bash
#
# jammo-loader
#
# This file is part of JamMo.
#
# (c) 2010 University of Oulu
#
# Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>

dif_file=$1
original_directory=$2
dif_directory=$3
timestamp=$(date +%s)

if [ "$dif_file" = "" -o "$original_directory" = "" -o "$dif_directory" = "" ]; then
	echo "Usage: $0 <dif_file> <original_directory> <dif_directory>"
	exit 1
fi

rm -f "${dif_file}.tmp"

function handle_directory {
	mkdir -p "$dif_directory/$1"

	for f in $(ls "$original_directory/$1"); do
		if [ -d "$original_directory/$1/$f" ]; then
			handle_directory "${1}$f/"
		else
			size=$(perl -e 'printf "%d", (stat(shift))[7];' "$original_directory/$1/$f")
			file_timestamp=$(perl -e 'printf "%d", (stat(shift))[9];' "$original_directory/$1/$f")
			original_timestamp=$(grep -s "^${1}$f " "$dif_file" | cut -d " " -f 2)
			if [ "$original_timestamp" = "" ]; then
				original_timestamp=0
			fi

			if [ $file_timestamp -gt $original_timestamp ]; then
				cp "$original_directory/$1/$f" "$dif_directory/$1/$f.$timestamp"
				echo "${1}$f" $timestamp $size >> "${dif_file}.tmp"
			else
				cp "$original_directory/$1/$f" "$dif_directory/$1/$f.$original_timestamp"
				echo "${1}$f" $original_timestamp $size >> "${dif_file}.tmp"
			fi
		fi
	done
}

handle_directory ""

mv "${dif_file}.tmp" "$dif_file"
