
/*
 * jammo-loader
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef _DIFPARSER_H_
#define _DIFPARSER_H_

#include <glib.h>

typedef struct _DifItem {
	gchar* path;
	time_t timestamp;
	size_t size;
} DifItem;

DifItem* dif_item_new(const gchar* path, time_t timestamp, size_t size);
void dif_item_free(DifItem* dif_item);

GList* parse_dif_file(const gchar* filename);
void free_dif_items(GList* dif_items);

#endif
