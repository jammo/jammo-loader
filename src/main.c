
/*
 * jammo-loader
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include <gtk/gtk.h>
#include <gst/gst.h>
#include "difparser.h"
#include "jammo-meam.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct {
	GList* next_dif_items;
	GstElement* pipeline;
	gchar* path;
} Data;

static const gchar* root_url;
static const gchar* root_path;
static GtkWidget* progress_bar;
static guint dif_item_index;
static guint n_dif_items;
static gint error_return = 0;

static void download_dif_items(GList* dif_items);

static void show_message(const gchar* message) {
	GtkWidget* message_dialog;
	
	message_dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", message);
	gtk_window_set_title(GTK_WINDOW(message_dialog), "JamMo Loader");
	
	gtk_dialog_run(GTK_DIALOG(message_dialog));
}

static gboolean on_dialog_delete_event(GtkWidget* widget, GdkEvent* event, gpointer user_data) {
	error_return = 2;
	gtk_main_quit();

	return FALSE;
}

static gboolean bus_callback(GstBus* bus, GstMessage* message, gpointer user_data) {
	Data* data;
	gboolean cleanup = FALSE;
	GstFormat format;
	gint64 duration;
	GList* next_dif_items;
	GError* error = NULL;
	GstObject* o;
	gchar* s;
	gchar* text;
	
	data = (Data*)user_data;
	cleanup = FALSE;
	if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS) {
		format = GST_FORMAT_TIME;
		if (gst_element_query_duration(data->pipeline, &format, &duration)) {
			jammo_meam_set_cached_duration(data->path, duration);
		}
		cleanup = TRUE;
	} else if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ERROR) {
		gst_message_parse_error(message, &error, NULL);
		if ((o = GST_MESSAGE_SRC(message)) && (g_object_get(o, "location", &s, NULL), s)) {
			text = g_strconcat(s, ": ", error->message, NULL);
			g_free(s);
		} else {
			text = g_strdup(error->message);
		}
		show_message(text);
		g_free(text);
		g_error_free(error);

		error_return = 1;
		cleanup = TRUE;
	}
	if (cleanup) {
		next_dif_items = data->next_dif_items;
		
		gst_element_set_state(data->pipeline, GST_STATE_NULL);
		gst_object_unref(data->pipeline);
		g_free(data->path);
		g_free(data);
		
		download_dif_items(next_dif_items);
	}

	return !cleanup;
}

static void on_new_decoded_pad(GstElement* element, GstPad* pad, gboolean last, gpointer data) {
	GstElement* sink;
	GstPad* sinkpad;
	GstCaps* caps;

	sink = GST_ELEMENT(data);
	sinkpad = gst_element_get_pad(sink, "sink");
	if (!GST_PAD_IS_LINKED(sinkpad)) {
		caps = gst_pad_get_caps(pad);
		if (g_strrstr(gst_structure_get_name(gst_caps_get_structure(caps, 0)), "audio")) {
			gst_pad_link(pad, sinkpad);
		}
		gst_caps_unref(caps);
	}
	g_object_unref(sinkpad);
}


static void download_dif_item(DifItem* dif_item, GList* next_dif_items) {
	Data* data;
	GstBus* bus;
	GstElement* src;
	GstElement* queue;
	GstElement* sink;
	GstElement* decodebin;
	GstElement* wavenc;
	gchar* s;
	gchar* uri;
	
	data = g_malloc0(sizeof(Data));
	data->next_dif_items = next_dif_items;
	data->pipeline = gst_element_factory_make("pipeline", NULL);
	data->path = g_strconcat(root_path, "/", dif_item->path, NULL);
	bus = gst_pipeline_get_bus(GST_PIPELINE(data->pipeline));
	gst_bus_add_watch(bus, bus_callback, data);
	gst_object_unref(bus);

	src = gst_element_factory_make("gnomevfssrc", NULL);
	s = g_uri_escape_string(dif_item->path, "/", FALSE);
	uri = g_strdup_printf("%s/%s.%lu", root_url, s, (unsigned long)dif_item->timestamp);
	g_object_set(src, "location", uri, NULL);
	g_free(uri);
	g_free(s);

	queue = gst_element_factory_make("queue", NULL);

	decodebin = gst_element_factory_make("decodebin", NULL);
	wavenc = gst_element_factory_make("wavenc", NULL);
	g_signal_connect(decodebin, "new-decoded-pad", G_CALLBACK(on_new_decoded_pad), wavenc);

	sink = gst_element_factory_make("filesink", NULL);
	g_object_set(sink, "location", data->path, NULL);
	
	gst_bin_add_many(GST_BIN(data->pipeline), src, queue, decodebin, wavenc, sink, NULL);
	gst_element_link_many(src, queue, decodebin, NULL);
	gst_element_link(wavenc, sink);

	gst_element_set_state(data->pipeline, GST_STATE_PLAYING); 
}

static void download_dif_items(GList* dif_items) {
	DifItem* dif_item;
	gchar* s;
	struct stat stat_buf;
	
	if (dif_items && !error_return) {
		dif_item = dif_items->data;
		
		gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(progress_bar), (gdouble)dif_item_index / n_dif_items);
		dif_item_index++;

		s = g_strconcat(dif_item->path, "...", NULL);
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(progress_bar), s);
		g_free(s);
		
		s = g_strconcat(root_path, "/", dif_item->path, NULL);
		if (stat(s, &stat_buf) ||
		    stat_buf.st_mtime < dif_item->timestamp ||
		    stat_buf.st_size == 0) {
		    	g_message("Downloading '%s'...", dif_item->path);
			download_dif_item(dif_item, g_list_next(dif_items));
		} else {
			download_dif_items(g_list_next(dif_items));
		}
		g_free(s);
	} else {
		gtk_main_quit();
	} 
}

static gboolean do_things(gpointer user_data) {
	const gchar* dif_file;
	GList* dif_items;

	dif_file = (const gchar*)user_data;
	if (!(dif_items = parse_dif_file(dif_file))) {
		show_message("Failed to install JamMo data files.");
		
		gtk_main_quit();
	} else {
		dif_item_index = 0;
		n_dif_items = g_list_length(dif_items);

		if (n_dif_items) {
			download_dif_items(dif_items);
		} else {
			gtk_main_quit();
		}
	}
	
	return FALSE;
}

int main(int argc, char** argv) {
	const gchar* dif_file;
	const gchar* duration_cache;
	GtkWidget* dialog;
	GtkWidget* label;

	nice(1);

	if (argc != 5) {
		g_printerr("Usage: %s <dif_file> <root_url> <root_path> <duration_cache>\n", (argc > 0 ? argv[0] : "jammo-loader"));
		
		return 1;
	}

	dif_file = argv[1];
	root_url = argv[2];
	root_path = argv[3];
	duration_cache = argv[4];
	
	gtk_init(&argc, &argv);
	jammo_meam_init(&argc, &argv, duration_cache);

	dialog = gtk_dialog_new();
	gtk_window_set_modal(GTK_WINDOW(dialog), FALSE);
	gtk_window_set_title(GTK_WINDOW(dialog), "JamMo Loader");
	g_signal_connect(dialog, "delete-event", G_CALLBACK(on_dialog_delete_event), NULL);
	
	label = gtk_label_new("Please wait while downloading and installing JamMo data files.");
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(dialog))), label);

	progress_bar = gtk_progress_bar_new();
	gtk_progress_bar_set_text(GTK_PROGRESS_BAR(progress_bar), "Parsing data index file...");
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(dialog))), progress_bar);
		
	gtk_widget_show_all(dialog);

	g_timeout_add(1, do_things, (gpointer)dif_file);

	gtk_main();
		
	jammo_meam_cleanup();

	return error_return;
}
