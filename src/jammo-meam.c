/*
 * jammo-meam.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-meam.h"

/*
	These are not used in loader.

#include "jammo-pipeline.h"
#include "gst-jammo-plugins/pitchdetect-plugin/gstjammopitchdetect.h"
#include "gst-jammo-plugins/sampler-plugin/gstjammosampler.h"
#include "gst-jammo-plugins/slider-plugin/gstjammoslider.h"
#include "gst-jammo-plugins/metronome-plugin/gstjammometronome.h"

*/

#include <gst/gst.h>
#include <gst/controller/gstcontroller.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/*
	These are not used in loader.

#include "../configure.h"

*/

static GHashTable* cached_durations;
static gchar* cached_durations_filename;
static gboolean cached_durations_changed;

static gboolean check_data_length(const gchar* pointer, gsize data_length, const gchar* contents, gsize contents_length, const gchar* filename) {
	gboolean valid = TRUE;
	
	if (pointer + sizeof(guint16) >= contents + contents_length) {
		g_critical("Corrupted duration cache file '%s'.", filename);
		valid = FALSE;
	}

	return valid;
}

static gboolean load_cached_durations(const gchar* filename) {
	gboolean retvalue = TRUE;
	gchar* contents;
	gsize length;
	gchar* pointer;
	const gchar* name;
	guint16 name_length;
	guint64 duration;
	
	if (!g_file_get_contents(filename, &contents, &length, NULL)) {
		g_warning("Could not load cached durations from a file '%s'.", filename);
		retvalue = FALSE;
	} else {
		pointer = contents; 
		while (pointer < contents + length) {
			if (!check_data_length(pointer, sizeof(guint16), contents, length, filename)) {
				retvalue = FALSE;
				break;
			}
			name_length = GUINT16_FROM_BE(*((guint16*)pointer));
			pointer += sizeof(guint16);

			if (!check_data_length(pointer, name_length + sizeof(guint64), contents, length, filename)) {
				retvalue = FALSE;
				break;
			}
			name = pointer;
			pointer += name_length;
			duration = GUINT64_FROM_BE(*((guint64*)pointer));
			pointer += sizeof(guint64);
			
			jammo_meam_set_cached_duration(name, duration);
		}
		
		g_free(contents);
	}
	
	return retvalue;
}

static void write_cached_duration_into_fd(gpointer key, gpointer value, gpointer user_data) {
	int fd;
	const gchar* name;
	gsize name_length;
	guint16 name_length_be;
	guint64 duration_be;

	fd = GPOINTER_TO_INT(user_data);

	name = (const gchar*)key;
	name_length = strlen(name);
	name_length_be = GUINT16_TO_BE(name_length);
	duration_be = GUINT64_TO_BE(*(guint64*)value);
	
	if (write(fd, &name_length_be, sizeof(guint16)) != sizeof(guint16) ||
	    write(fd, name, name_length) != name_length ||
	    write(fd, &duration_be, sizeof(guint64)) != sizeof(guint64)) {
		g_critical("Could not write a cached duration into a file.");
	}
}

static void save_cached_durations(const gchar* filename) {
	int fd;
	
	if ((fd = creat(filename, 0666)) == -1) {
		g_critical("Could not save cached durations into a file '%s'.", filename);
	} else {
		g_hash_table_foreach(cached_durations, write_cached_duration_into_fd, GINT_TO_POINTER(fd));
		close(fd);
	}
}

void jammo_meam_init(int* argc, char** argv[], const gchar* duration_cache_filename) {
	gchar* path;
	
	gst_init(argc, argv);
	
/*
	These are not used in loader.

	gst_controller_init (NULL, NULL);
	
	gst_element_register(NULL, "jammopipeline", 0, JAMMO_TYPE_PIPELINE);
	gst_element_register(NULL, "pitchdetect", 0, GST_TYPE_PITCHDETECT);
	gst_element_register(NULL, "jammosampler", 0, GST_TYPE_JAMMO_SAMPLER);
	gst_element_register(NULL, "jammoslider", 0, GST_TYPE_JAMMO_SLIDER);
	gst_element_register(NULL, "jammometronome", 0, GST_TYPE_JAMMO_METRONOME);
	
*/

	cached_durations = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);

	if (duration_cache_filename[0] == '/') {
		path = g_strdup(duration_cache_filename);

		cached_durations_filename = g_strdup(duration_cache_filename);

/*
	These are not used in loader.

	} else {
		path = g_build_filename(JAMMO_DIRECTORY, duration_cache_filename, NULL);
		if (!g_file_test(path, G_FILE_TEST_EXISTS)) {
			g_free(path);
			path = g_build_filename(DATA_DIR, duration_cache_filename, NULL);
		}

		cached_durations_filename = g_build_filename(JAMMO_DIRECTORY, duration_cache_filename, NULL);
*/

	}
	if (load_cached_durations(path)) {
		cached_durations_changed = FALSE;
	} else {
		cached_durations_changed = TRUE;
	}
	g_free(path);
}

void jammo_meam_cleanup(void) {
	gst_deinit();
	
	if (cached_durations_changed) {
		save_cached_durations(cached_durations_filename);
	}
	g_hash_table_destroy(cached_durations);
	g_free(cached_durations_filename);
}

guint64 jammo_meam_get_cached_duration(const gchar* filename) {
	guint64 duration = JAMMO_DURATION_INVALID;
	guint64* duration_pointer;
	
	if (filename && (duration_pointer = g_hash_table_lookup(cached_durations, filename))) {
		duration = *duration_pointer;
	}
	
	return duration;
}

void jammo_meam_set_cached_duration(const gchar* filename, guint64 duration) {
	guint64* duration_pointer;

	duration_pointer = g_malloc(sizeof(guint64));
	*duration_pointer = duration;
	g_hash_table_insert(cached_durations, g_strdup(filename), duration_pointer);
	
	cached_durations_changed = TRUE;
}
