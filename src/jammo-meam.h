/*
 * jammo-meam.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef INCLUDED_JAMMO_MEAM_H
#define INCLUDED_JAMMO_MEAM_H

#include <glib.h>

#define JAMMO_DURATION_INVALID ((guint64)-1)

void jammo_meam_init(int* argcp, char** argvp[], const gchar* duration_cache_filename);
void jammo_meam_cleanup(void);

guint64 jammo_meam_get_cached_duration(const gchar* filename);
void jammo_meam_set_cached_duration(const gchar* filename, guint64 duration);

#endif
