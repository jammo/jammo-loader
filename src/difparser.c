
/*
 * jammo-loader
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "difparser.h"
#include <string.h>

static DifItem* parse_dif_line(const gchar* line);

DifItem* dif_item_new(const gchar* path, time_t timestamp, size_t size) {
	DifItem* dif_item;
	
	dif_item = g_new(DifItem, 1);
	dif_item->path = g_strdup(path);
	dif_item->timestamp = timestamp;
	dif_item->size = size;
	
	return dif_item;
}

void dif_item_free(DifItem* dif_item) {
	g_free(dif_item->path);
	g_free(dif_item);
}

GList* parse_dif_file(const gchar* filename) {
	GList* dif_items = NULL;
	gchar* contents;
	GError* error = NULL;
	gchar* start;
	gchar* end;
	gchar* line;
	DifItem* dif_item;

	if (!g_file_get_contents(filename, &contents, NULL, &error)) {
		g_critical("Unable to read the dif file '%s': %s", filename, error->message);
		g_error_free(error);
	} else {
		for (start = end = contents; (end = strchr(start, '\n')); start = end + 1) {
			line = g_strndup(start, end - start);
			if ((dif_item = parse_dif_line(line))) {
				dif_items = g_list_prepend(dif_items, dif_item);
			}
			g_free(line);
		}

		g_free(contents);

		dif_items = g_list_reverse(dif_items);
	}

	return dif_items;
}

void free_dif_items(GList* dif_items) {
	g_list_foreach(dif_items, (GFunc)dif_item_free, NULL);
	g_list_free(dif_items);
}

static DifItem* parse_dif_line(const gchar* line) {
	DifItem* dif_item = NULL;
	gchar** tokens;
	time_t timestamp;
	size_t size;
	
	tokens = g_strsplit(line, " ", 3);
	if (g_strv_length(tokens) != 3 ||
	    !(timestamp = (time_t)g_ascii_strtoull(tokens[1], NULL, 0)) ||
	    !(size = (size_t)g_ascii_strtoull(tokens[2], NULL, 0))) {
		g_critical("Error when parsing the line: %s", line);
	} else {
		dif_item = dif_item_new(tokens[0], timestamp, size);
	}

	return dif_item;
}
