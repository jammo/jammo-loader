#! /bin/bash
#
# jammo-loader
#
# This file is part of JamMo.
#
# (c) 2010 University of Oulu
#
# Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>

dif_file=$1
root_url=$2

if [ "$dif_file" = "" -o "$root_url" = "" ]; then
	echo "Usage: $0 <dif_file> <root_url>"
	exit 1
fi

IFS=$'\n'
for f in $(cat $dif_file); do
	echo -n "."
	file=$(echo $f | cut -d " " -f 1 | sed 's/#/%23/g')
	timestamp=$(echo $f | cut -d " " -f 2)
	url=${root_url}/${file}.${timestamp}
	if ! curl -fs -I $url > /dev/null; then
		echo ""
		echo "Error: $url" > /dev/stderr
	fi
done
echo ""
